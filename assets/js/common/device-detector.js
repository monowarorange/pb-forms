const root = document.documentElement;
let bodyEl = document.body;
if (screen.width >= 1025) {
  bodyEl.classList.add('desk_styles');
  root.classList.add('desktop');
  console.log('desk');
} else if (screen.width >= 768) {
  bodyEl.classList.add('tab_styles');
  root.classList.add('tablet');
  console.log('tab');
} else {
  bodyEl.classList.add('mobile_styles');
  root.classList.add('mobile');
  console.log('mobile');
}
root.classList.add('default');
